﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;
using System.Windows.Controls;
using System.Windows.Input;

using CEMTBasicUI.Helpers;
using CEMTBasicUI.Views;


namespace CEMTBasicUI.ViewModels
{

    enum VisibilityOptions
    {
        [EnumMember(Value = "Hidden")]
        hidden,

        [EnumMember(Value = "Visible")]
        visible,
    }

    class MainWindowViewModel: BaseViewModel
    {
        private string _showToolTip;
        public string ShowToolTip { 
            get { return _showToolTip; } 
            set {
                _showToolTip = value;
                OnPropertyChanged(nameof(ShowToolTip)); 
            } 
        }
        public ICommand ToggleSideBarCommand { get; }

        public UserControlManager UserControl { get; }

        public MainWindowViewModel()
        {
            UserControl = new UserControlManager(new List<UserControl> {
                new Overview(),
                new Tenants(),
                new Endpoints(),
                new Alerts(),
                new Settings(),
            });
            UserControl.ShowUserControlByClass("Overview");

            ShowToolTip = VisibilityOptions.hidden.ToString();
            ToggleSideBarCommand = new RelayCommand(
                (param) => ToggleToolTipDisplay()
            );
        }

        public void ToggleToolTipDisplay()
        {
            if (ShowToolTip.Equals(VisibilityOptions.hidden.ToString()))
                ShowToolTip = VisibilityOptions.visible.ToString();
            else
                ShowToolTip = VisibilityOptions.hidden.ToString();
        }
    }
}
