﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace CEMTBasicUI.Components
{

    public partial class NavLink : UserControl
    {

        public static readonly DependencyProperty LinkTextProperty = DependencyProperty.Register(
            "LinkText", typeof(string), typeof(NavLink), new PropertyMetadata(String.Empty)
        );
        public string LinkText {
            get { return (string)GetValue(LinkTextProperty); }
            set { SetValue(LinkTextProperty, value); }
        }

        public static readonly DependencyProperty IconSourceProperty = DependencyProperty.Register(
            "IconSource", typeof(ImageSource), typeof(NavLink)
        );
        public ImageSource IconSource
        {
            get { return (ImageSource)GetValue(IconSourceProperty); }
            set { SetValue(IconSourceProperty, value); }
        }

        public static readonly DependencyProperty ShowUserControlCommandProperty = DependencyProperty.Register(
            "ShowUserControlCommand", typeof(ICommand), typeof(NavLink)
        );
        public ICommand ShowUserControlCommand
        {
            get { return (ICommand)GetValue(ShowUserControlCommandProperty); }
            set { SetValue(ShowUserControlCommandProperty, value); }
        }

        public static readonly DependencyProperty TargetUserControlProperty = DependencyProperty.Register(
            "TargetUserControl", typeof(string), typeof(NavLink)
        );
        public string TargetUserControl
        {
            get { return (string)GetValue(TargetUserControlProperty); }
            set { SetValue(TargetUserControlProperty, value); }
        }


        public static readonly DependencyProperty IsActiveProperty = DependencyProperty.Register(
            "IsActive", typeof(object), typeof(NavLink)
        );
        public object IsActive
        {
            get { return (object)GetValue(IsActiveProperty); }
            set { SetValue(IsActiveProperty, value); }
        }

        public static readonly DependencyProperty ShowToolTipProperty = DependencyProperty.Register(
            "ShowToolTip", typeof(string), typeof(NavLink)
        );
        public string ShowToolTip
        {
            get { return (string)GetValue(ShowToolTipProperty); }
            set { SetValue(ShowToolTipProperty, value); }
        }

        public NavLink()
        {
            InitializeComponent();

            this.nav_link.DataContext = this;
        }

    }
}
